from django.test import TestCase, LiveServerTestCase
from django.test.client import Client
from django.urls import resolve
from .views import index
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time

# Create your tests here.

class Story8UnitTest(TestCase):
    def test_story8_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code,200)

    def test_story8_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'index.html')

    def test_story8_func(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

class Story8FunctionalTest(LiveServerTestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.driver = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(Story8FunctionalTest,self).setUp()

    def tearDown(self):
        self.driver.quit()
        super(Story8FunctionalTest,self).tearDown()

    def test_accordion(self):
        self.driver.get(self.live_server_url)
        response_page = self.driver.page_source

        self.assertIn("About Me", response_page)
        self.assertIn("Current Activities", response_page)
        self.assertIn("Experiences", response_page)
        self.assertIn("Achievements", response_page)

    def test_accordion_can_show_content(self):
        self.driver.get(self.live_server_url)
        response_content = self.driver.page_source

        accordion_bar = self.driver.find_element_by_id('one')
        self.assertFalse(accordion_bar.get_attribute('aria-expanded'))

        accordion_bar.click()
        time.sleep(3)
        self.assertTrue(accordion_bar.get_attribute('aria-expanded'))


    def test_up_and_down_arrows(self) :
        self.driver.get(self.live_server_url)
        response_page = self.driver.page_source

        up = self.driver.find_element_by_id('one-button').find_element_by_id('oneUp')
        down = self.driver.find_element_by_id('one-button').find_element_by_id('oneDown')

        about_me = response_page.find('About Me')
        activity = response_page.find('Activity')
        self.assertTrue(about_me > activity)

        time.sleep(2)
        down.click()
        response_page = self.driver.page_source

        time.sleep(2)
        about = response_page.find('About Me')
        activity = response_page.find('Activity')
        self.assertTrue(about_me > activity)

        time.sleep(2)
        up.click()

        time.sleep(2)
        response_page = self.driver.page_source
        about = response_page.find('About Me')
        activity = response_page.find('Activity')
        self.assertTrue(about_me > activity)

    def test_change_color_theme(self):
        self.driver.get(self.live_server_url)
        response_page = self.driver.page_source

        light_bg_color = self.driver.find_element_by_class_name('light').value_of_css_property('background-color')
        self.assertEqual(light_bg_color, 'rgba(220, 237, 193, 0.8)')

        light_title_text_color = self.driver.find_element_by_class_name('light').value_of_css_property('color')
        self.assertEqual(light_title_text_color, 'rgba(0, 0, 0, 1)')

        light_box_color = self.driver.find_element_by_class_name('moodBoard').value_of_css_property('background-color')
        self.assertEqual(light_box_color, 'rgba(254, 247, 229, 1)')

        light_accordion_bar_color = self.driver.find_element_by_class_name('bar').value_of_css_property('background-color')
        self.assertEqual(light_accordion_bar_color, 'rgba(255, 211, 182, 1)')

        light_up_color = self.driver.find_element_by_class_name('move-up').value_of_css_property('background-color')
        self.assertEqual(light_up_color, 'rgba(255, 211, 182, 1)')

        light_down_color = self.driver.find_element_by_class_name('move-down').value_of_css_property('background-color')
        self.assertEqual(light_down_color, 'rgba(255, 211, 182, 1)')

        light_desc_color = self.driver.find_element_by_class_name('desc').value_of_css_property('color')
        self.assertEqual(light_desc_color, 'rgba(0, 0, 0, 1)')

        time.sleep(2)
        self.driver.find_element_by_tag_name('label').click()
        time.sleep(2)

        night_bg_color = self.driver.find_element_by_class_name('night').value_of_css_property('background-color')
        self.assertEqual(night_bg_color, 'rgba(0, 0, 0, 1)')

        night_title_text_color = self.driver.find_element_by_class_name('night').value_of_css_property('color')
        self.assertEqual(night_title_text_color, 'rgba(245, 245, 245, 1)')

        night_box_color = self.driver.find_element_by_class_name('moodBoard').value_of_css_property('background-color')
        self.assertEqual(night_box_color, 'rgba(245, 245, 245, 1)')

        night_accordion_bar_color = self.driver.find_element_by_class_name('bar').value_of_css_property('background-color')
        self.assertEqual(night_accordion_bar_color, 'rgba(0, 0, 0, 1)')

        night_up_color = self.driver.find_element_by_class_name('move-up').value_of_css_property('background-color')
        self.assertEqual(night_up_color, 'rgba(0, 0, 0, 1)')

        night_down_color = self.driver.find_element_by_class_name('move-down').value_of_css_property('background-color')
        self.assertEqual(night_down_color, 'rgba(0, 0, 0, 1)')

        night_desc_color = self.driver.find_element_by_class_name('desc').value_of_css_property('color')
        self.assertEqual(night_desc_color, 'rgba(128, 128, 128, 1)')
