jQuery('.move').children().click(function (e) {
    var $div = $(this).closest('.item');
    if (jQuery(e.target).is('.move-down')) { 
        $div.next('.item').after($div);
    } else {
        $div.prev('.item').before($div);
    }
});